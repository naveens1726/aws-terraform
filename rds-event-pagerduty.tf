resource "aws_iam_policy" "rds_event_pagerduty_policy" {
  name        = "rds_event_sns_pagerduty_policy"
  description = "Policy for sns rds event "

  policy = <<EOT
{
  "Version": "2008-10-17",  
  "Statement": [
    {      
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
      ],
      "Resource": "${aws_sns_topic.rds_event_pagerduty.arn}",
    }
  ]
}
EOT
}

resource "aws_sns_topic_policy" "rds_event" {
  arn    = aws_sns_topic.rds_event_pagerduty.arn
  policy = aws_iam_policy_document.rds_event_pagerduty_policy.policy
}

resource "aws_sns_topic_subscription" "pagerduty_subscription" {
  topic_arn = aws_sns_topic.rds_event_pagerduty.arn
  protocol  = "https"
  endpoint  = ""
}
